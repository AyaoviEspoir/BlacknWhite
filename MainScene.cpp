#include "MainScene.h"
#include "GameElements\CellPanel.h"
#include "GameElements\ScoreLabel.h"
#include "Utilities\Base.h"

USING_NS_CC;

using namespace cocostudio::timeline;

Main::Main()
{

}

Main::~Main()
{

}

Scene* Main::createScene()
{
    // 'scene' is an autorelease object
    auto scene = Scene::create();
    
    // 'layer' is an autorelease object
    auto layer = Main::create();

    // add layer as a child to scene
    scene->addChild(layer);

    // return the scene
    return scene;
}

// on "init" you need to initialize your instance
bool Main::init()
{
    //////////////////////////////
    // 1. super init first
    if ( !Layer::init() )
    {
        return false;
    }
    
    _rootNode = CSLoader::createNode("MainScene.csb");

    addChild(_rootNode);

	initializeCells();

	initializeScores();

    return true;
}

void Main::initializeCells()
{
	auto cellPanel = CellPanel::createPanel((cocos2d::ui::Layout *)_rootNode);
}

void Main::initializeScores()
{
	_aiScoreLabel = (cocos2d::ui::Text *)_rootNode->getChildByName("ai_score");
	
	auto aiScore = ScoreLabel::createScoreLabel(_aiScoreLabel);
	
	_playerScoreLabel = (cocos2d::ui::Text *)_rootNode->getChildByName("player_score");
	
	auto playerScore = ScoreLabel::createScoreLabel(_playerScoreLabel);
}
