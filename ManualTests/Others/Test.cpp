#include <iostream>
#include <vector>

void modify(std::vector<int> & x)
{
	x.push_back(4);
}

int main(int argc, char** argv)
{
	std::vector<int> x = {1, 2};

	modify(x);

	std::cout << x.size() << std::endl;

	return 0;
}