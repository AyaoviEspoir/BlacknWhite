#include "..\..\Logic\CCell.h"

/*
A helper function to print out the content of a vector.
*/
void print(std::vector<int> vec)
{
	for (int i : vec)
	{
		std::cout << "----------- " << i << std::endl;
	}
}

int main(int argc, char** argv)
{
	CCell * cell = new CCell();

	std::cout << "===================================================" << std::endl;

	std::cout << "Testing getCornerCellNeighbours..." << std::endl;

	std::cout << "Position is 0" << std::endl;

	std::vector<int> neighbours = cell->getNeighbours(0);

	std::cout << neighbours.size() << std::endl;		/* should be 3. */

	print(neighbours);

	std::cout << "Position is 19" << std::endl;

	neighbours = cell->getNeighbours(19);

	std::cout << neighbours.size() << std::endl;		/* should be 5. */

	print(neighbours);

	std::cout << "Position is 380" << std::endl;

	neighbours = cell->getNeighbours(380);

	std::cout << neighbours.size() << std::endl;		/* should be 5. */

	print(neighbours);

	std::cout << "Position is 399" << std::endl;

	neighbours = cell->getNeighbours(399);

	std::cout << neighbours.size() << std::endl;		/* should be 5. */

	print(neighbours);

	std::cout << "===================================================" << std::endl;

	std::cout << "Testing getEdgeCellNeighbours..." << std::endl;

	std::cout << "Position is 17" << std::endl;

	neighbours = cell->getNeighbours(17);

	std::cout << neighbours.size() << std::endl;		/* should be 5. */

	print(neighbours);

	std::cout << "Position is 385" << std::endl;

	neighbours = cell->getNeighbours(385);

	std::cout << neighbours.size() << std::endl;		/* should be 5. */

	print(neighbours);

	std::cout << "Position is 100" << std::endl;

	neighbours = cell->getNeighbours(100);

	std::cout << neighbours.size() << std::endl;		/* should be 5. */

	print(neighbours);

	std::cout << "Position is 219" << std::endl;

	neighbours = cell->getNeighbours(219);

	std::cout << neighbours.size() << std::endl;		/* should be 5. */

	print(neighbours);

	std::cout << "===================================================" << std::endl;

	std::cout << "Testing getInnerCellNeighbours..." << std::endl;

	std::cout << "Position is 35" << std::endl;

	neighbours = cell->getNeighbours(35);

	std::cout << neighbours.size() << std::endl;		/* should be 5. */

	print(neighbours);

	return 0;
}

