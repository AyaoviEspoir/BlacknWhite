#ifndef __MAIN_SCENE_H__
#define __MAIN_SCENE_H__

#include "cocos2d.h"
#include "cocostudio/CocoStudio.h"
#include "ui/CocosGUI.h"

class Main: public cocos2d::Layer
{

private:
	
	cocos2d::ui::Text * _aiScoreLabel;		/* Hosts the ai score. */
	
	cocos2d::ui::Text * _playerScoreLabel;	/* Hosts the player score. */
	
	//cocos2d::ui::Layout * _cellsPanel;		/* Hosts the 20x20 cells for the game. */
	
	cocos2d::Node * _rootNode;				/* Is the main layer. */
	
	/*
	Creates cellsPanel passes it the _rootNode onto which will be 
	loaded the cells internally in cellsPanel .
	*/
	void initializeCells();

	/*
	Retrieves the text labels from the scene and makes use of them.
	*/
	void initializeScores();

public:
	Main();

	~Main();

    // there's no 'id' in cpp, so we recommend returning the class instance pointer
    static cocos2d::Scene* createScene();

    // Here's a difference. Method 'init' in cocos2d-x returns bool, instead of returning 'id' in cocos2d-iphone
    virtual bool init();

    // implement the "static create()" method manually
    CREATE_FUNC(Main);
};

#endif // __MAIN_SCENE_H__
