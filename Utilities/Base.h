#pragma once

#include <iostream>
#include <string>

static const std::string WHITE_CELL_BLOCK = "white_block_20x20.png";
static const std::string BLACK_CELL_BLOCK = "black_block_20x20.png";
static const std::string RED_CELL_BLOCK = "red_block_20x20.png";
static const std::string GREEN_CELL_BLOCK = "green_block_20x20.png";
static const std::string ORANGE_CELL_BLOCK = "orange_block_20x20.png";
static const std::string TRANSPARENT_CELL_BLOCK = "transparent_block_20x20.png";
static const std::string CELL_PANEL_BACKGROUND = "transparent_panel_480x480.png";

static const std::string BOMB_ICON = "bomb_20x20.png";
static const std::string HAMMER_ICON = "hammer_20x20.png";
static const std::string SHIELD_ICON = "shield_20x20.png";

static const short int NUMBER_OF_ROWS = 20;
static const short int NUMBER_OF_COLUMNS = 20;
static const short int NUMBER_OF_CELLS_PER_ROW = 20;

/*
Used to fit the cell grid on the screen perfectly.
*/
static const short int ROW_STARTING_POSITION = 12;
static const short int COLUMN_STARTING_POSITION = 168;

static const short int GAAP_BETWEEN_CELLS = 4;
static const short int WIDTH_OF_CELL = 20;

static const short int CELL_COUNTER = 0;
static const short int TOTAL_NUMBER_OF_CELLS = 400;

static const short int CELL_PANEL_POSITION_X = 160;
static const short int CELL_PANEL_POSITION_Y = 10;

/*
Used to locate the corners.
*/
static const short int BOTTOM_LEFT_CORNER = 0;
static const short int BOTTOM_RIGHT_CORNER = 19;
static const short int TOP_LEFT_CORNER = 380;
static const short int TOP_RIGH_CORNER = 399;

/*
Used to locate the top and bottom rows.
*/
static const short int BOTTOM_ROW = 0;
static const short int TOP_ROW = 19;

/*
Used to locate the left and right edges.
*/
static const short int LEFT_EDGE = 0;
static const short int RIGHT_EDGE = 19;

enum State { DISABLED, ENABLED, OWNED, SELECTED };

enum CellOwner { TheAI, NOONE, PLAYER };

enum Notification { NOTIFIED, UNNOTIFIED };

static const short int MINIMUM_NUMBER_OF_CELL_FOR_SCORING = 3;

static const short int MAXIMUM_NUMBER_OF_NEIGHBOURS = 8;		/* Case of an inner-grid cell. */

static const short int AVERAGE_NUMBER_OF_NEIGHBOURS = 5;		/* Case of an edge cell. */

static const short int MINIMUM_NUMBER_OF_NEIGHBOURS = 3;		/* Case of a corner cell. */