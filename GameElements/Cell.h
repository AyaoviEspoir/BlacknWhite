#pragma once

#include "cocos2d.h"
#include "Utilities\Base.h"
#include "Logic\CCell.h"

class Cell : public cocos2d::Sprite
{
private:
	cocos2d::Vec2 _position;

	int _index;

	CCell * _ccell;

	std::vector<int> _internalCellsPosition;

	std::vector<int> _neighbours;

	State _cellState;			/* Defines the state od the cell; ENABLED, DISABLED or SELECTED. */

	CellOwner _cellOwner;

	//std::function<void(std::vector<int>)> _notificationCallback;

	/*
	Provides a way to notify a cell's neighbour for selection.
	*/
	void notifyNeighbours();

	/*
	Used as a helper of the notifyNeighbour function.
	*/
	void notificationHelper(int cellIndex);

public:

	Cell(cocos2d::Vec2 position);		/* Constructor. */

	~Cell();	/* Destructor. */

	static Cell* createCell(cocos2d::Vec2 position);
		
	virtual bool onTouchBegan(cocos2d::Touch* touch, cocos2d::Event* event);
    virtual void onTouchEnded(cocos2d::Touch* touch, cocos2d::Event* event);
    virtual void onTouchMoved(cocos2d::Touch* touch, cocos2d::Event* event);
    virtual void onTouchCancelled(cocos2d::Touch* touch, cocos2d::Event* event);
    
    /*
     * Initialses parameters such as position, tags and ...
     * */
    void initOptions();

	/*
	Provides a way to disable the cell; ie not up for selection.
	*/
	void disable();

	/*
	Provides a way to enable the cell; ie up for selection.
	*/
	void enable();

	/*
	Provides a way to select the cell.
	*/
	void select();

	/*
	Provides a way to get the state of the cell.
	*/
	State getState();

	/*
	Provides a way to set the state of the cell.
	*/
	void setState(State newState);

	/*
	Provides a way for this cell to be nootified.
	*/
	void notify(std::vector<int> cellsPosition);		/* Would like to modify add more positions to the vector 
														and the change be reflected at the base. */
	/*
	Provides a way to get the cell's index in the containning grid.
	*/
	int getIndex();

	/*
	Provides a way to get the owner of the cell.
	*/
	CellOwner getOwner();

	/*
	Provides a way to get the owner of the cell whose index has been given as argument.
	*/
	CellOwner getOwner(int cellIndex);

	/*
	Gets called at the end of a notification chain:
	(the current cell does not belong to the same owner)
	(the current cell has not been selected.)
	*/
	void notificationFollowUp();

	/*
	Takes cell texture change action.
	*/
	void reflectOwnership();

	/*
	Checks whether the given cellIndex is owned.
	*/
	bool isOwned(int cellIndex);

	/*
	Provides a way to notify all of a cell's neighbours.
	*/
	void notifyAllNeighbours();

	/*
	Returns a std::map<int, std::vector<int>> specifying 
	the different alignment present.
	*/
	std::map<int, std::vector<int>> checkMiddleMan();

	/*
	Helper function.
	*/
	bool isHorizontalMiddleMan();

	/*
	Helper function.
	*/
	bool isVerticalMiddleMan();

	/*
	Helper function.
	*/
	bool isRisingDiagonalMiddleMan();

	/*
	Helper function.
	*/
	bool isFallingDiagonalMiddleMan();

	/*
	Helper function.
	*/
	void furtherNeighbourSearchHelper(std::vector<int> & alignment, int step);

	/*
	Removes given cell index from neighbours.
	*/
	void removeFromNeighbours(int cellIndex);

	/*
	Simply put scattered cells together so they could be highlighted.
	*/
	void takeOwnershipAction(std::map<int, std::vector<int>> & alignments);

	/*
	Does the middleman check and takes appriopriate actions.
	*/
	void middleManCheckRoutine();

	/*
	A helper function.
	*/
	void checkMiddleManHelper(std::map<int, std::vector<int>> & alignments, int alignmentIndex, int precedingCellIndex, int hindmostCellIndex);
};
