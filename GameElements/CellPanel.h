#pragma once

#include "cocos2d.h"
#include "ui/CocosGUI.h"

class CellPanel
{

private:
	cocos2d::ui::Layout * _panel;

public:
	/* 
	Constructor. 
	*/
	CellPanel(cocos2d::ui::Layout * existingPanel);

	/* 
	Destructor. 
	*/
	~CellPanel();	

	/*
	Creates a panel from an existing panel.
	*/
	static CellPanel * createPanel(cocos2d::ui::Layout * existingPanel);

	/*
	Populates the panel with cells.
	*/
	void loadCells();
};