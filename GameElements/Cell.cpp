#include "Cell.h"

USING_NS_CC;

Cell::Cell(Vec2 position)
{
	_position = position;

	_ccell = new CCell();
	
	_cellState = ENABLED;

	_cellOwner = NOONE;

	auto touchListener = EventListenerTouchOneByOne::create();

	touchListener->onTouchBegan = CC_CALLBACK_2(Cell::onTouchBegan, this);
	touchListener->onTouchEnded = CC_CALLBACK_2(Cell::onTouchEnded, this);
	touchListener->onTouchMoved = CC_CALLBACK_2(Cell::onTouchMoved, this);
	touchListener->onTouchCancelled = CC_CALLBACK_2(Cell::onTouchCancelled, this);

	Director::getInstance()->getEventDispatcher()->addEventListenerWithFixedPriority(touchListener, 5);
}

Cell::~Cell()
{
	_neighbours.clear();

	_internalCellsPosition.clear();
}

Cell * Cell::createCell(Vec2 position)
{
	Cell * cell = new Cell(position);
	
	if (cell->initWithFile(ORANGE_CELL_BLOCK))
    {
        cell->autorelease();

        cell->initOptions();

        return cell;
    }

    CC_SAFE_DELETE(cell);

    return NULL;
}

void Cell::initOptions()
{
	this->setPosition(_position);
}

bool Cell::onTouchBegan(Touch* touch, Event* event)
{
	if (this->getBoundingBox().containsPoint(touch->getLocation()) && (_cellState == ENABLED))
	{
		this->setTexture(BLACK_CELL_BLOCK);
		
		return true;
	}

	return false;
}

void Cell::onTouchEnded(Touch* touch, Event* event)
{
	if (this->getBoundingBox().containsPoint(touch->getLocation()) && (_cellState == ENABLED))
	{
		_index = getIndex();
		
		_cellState = SELECTED;

		_cellOwner = PLAYER;
				
		_internalCellsPosition = { _index };

		notifyNeighbours();
	}
}

void Cell::onTouchMoved(Touch* touch, Event* event)
{
	//TODO
}

void Cell::onTouchCancelled(Touch* touch, Event* event)
{
	//TODO
}

void Cell::enable()
{
	_cellState = ENABLED;
}

void Cell::disable()
{
	_cellState = DISABLED;
}

void Cell::select()
{
	_cellState = SELECTED;

	_cellOwner = TheAI;
}

State Cell::getState()
{
	return _cellState;
}

void Cell::setState(State newState)
{
	_cellState = newState;
}

void Cell::notify(std::vector<int> cellsPosition)
{	
	bool alreadyNotified = (std::find(cellsPosition.begin(), cellsPosition.end(), this->getIndex()) != cellsPosition.end());

	if (alreadyNotified)
	{
		return;
	}

	if ((_cellState == ENABLED) || (_cellOwner != getOwner(cellsPosition[0])))
	{
		_internalCellsPosition = cellsPosition;

		notificationFollowUp();

		return;
	}
	
	if (((_cellState == SELECTED) || ((_cellState == OWNED) && (_cellOwner == getOwner(cellsPosition[0])))) && !alreadyNotified)
	{
		_internalCellsPosition = cellsPosition;

		_internalCellsPosition.push_back(getIndex());

		notifyNeighbours();
	}
}

int Cell::getIndex()
{
	int index;

	std::stringstream ss(this->getName());

	ss >> index;

	return index;
}

void Cell::notifyNeighbours()
{
	_neighbours = _ccell->getNeighbours(getIndex());

	/*
	There are few situations to be considered here:
	- The new SELECTED cell is in the middle of two or more 
	OWNED cells.
	- The new SELECTED cell is in the middle of two or more
	SELCTED cells.
	- And more as we go along.
	*/

	if (_neighbours.size() == MAXIMUM_NUMBER_OF_NEIGHBOURS)
	{
		middleManCheckRoutine();
	}

	else if (_neighbours.size() == AVERAGE_NUMBER_OF_NEIGHBOURS)
	{
		middleManCheckRoutine();
	}

	else if (_neighbours.size() == MINIMUM_NUMBER_OF_NEIGHBOURS)
	{
		notifyAllNeighbours();
	}
}

void Cell::middleManCheckRoutine()
{
	std::map<int, std::vector<int>> alignments = checkMiddleMan();

	if (alignments.size() != 0)
	{
		takeOwnershipAction(alignments);
	}

	notifyAllNeighbours();
}

void Cell::notifyAllNeighbours()
{
	for (int cell : _neighbours)
	{
		if ((_cellState == OWNED) && isOwned(cell))
		{
			continue;	/* Don not border notifying him. */
		}

		else
		{
			notificationHelper(cell);
		}
	}
}

void Cell::notificationHelper(int cellIndex)
{
	auto neighourCell = (Cell *) this->getParent()->getChildByName(std::to_string(cellIndex));

	neighourCell->notify(_internalCellsPosition);
}

CellOwner Cell::getOwner()
{
	return _cellOwner;
}

CellOwner Cell::getOwner(int cellIndex)
{
	auto cell = (Cell *) this->getParent()->getChildByName(std::to_string(cellIndex));

	return cell->getOwner();
}

void Cell::notificationFollowUp()
{
	if (_internalCellsPosition.size() < MINIMUM_NUMBER_OF_CELL_FOR_SCORING)
	{
		_internalCellsPosition.clear();
	}
	else
	{
		reflectOwnership();
	}
}

void Cell::reflectOwnership()
{
	if (_cellState != ENABLED)
	{
		/*
		This check is required because every cell; SELECTED or not 
		performs this check for middleman and if this cell is simply 
		ENABLED there is not point in letting it reflect ownership.
		*/

		for (int cellIndex : _internalCellsPosition)
		{
			auto cell = (Cell *) this->getParent()->getChildByName(std::to_string(cellIndex));

			cell->setTexture(RED_CELL_BLOCK);

			cell->setState(OWNED);
		}
	}

	_internalCellsPosition.clear();
}

bool Cell::isOwned(int cellIndex)
{
	auto cell = (Cell *) this->getParent()->getChildByName(std::to_string(cellIndex));

	return (cell->getState() == OWNED);
}

std::map<int, std::vector<int>> Cell::checkMiddleMan()
{
	std::map<int, std::vector<int>> alignments;

	/* Case vertical middleman. */
	if (isVerticalMiddleMan())
	{
		checkMiddleManHelper(alignments, 0, (_index + NUMBER_OF_CELLS_PER_ROW), (_index - NUMBER_OF_CELLS_PER_ROW));
	}

	/* Case falling diagonal middleman. */
	if (isFallingDiagonalMiddleMan())
	{
		checkMiddleManHelper(alignments, 1, (_index + NUMBER_OF_CELLS_PER_ROW - 1), (_index - NUMBER_OF_CELLS_PER_ROW + 1));
	}

	/* Case horizontal middleman. */
	if (isHorizontalMiddleMan())
	{
		checkMiddleManHelper(alignments, 2, (_index + 1), (_index - 1));
	}

	/* Case rising diagonal middleman. */
	if (isRisingDiagonalMiddleMan())
	{
		checkMiddleManHelper(alignments, 3, (_index + NUMBER_OF_CELLS_PER_ROW + 1), (_index - NUMBER_OF_CELLS_PER_ROW - 1));
	}

	return alignments;
}

void Cell::checkMiddleManHelper(std::map<int, std::vector<int>> & alignments, int alignmentIndex, int priorCellIndex, int hindmostCellIndex)
{
	int steps[] = { NUMBER_OF_CELLS_PER_ROW, (NUMBER_OF_CELLS_PER_ROW - 1), 1, (NUMBER_OF_CELLS_PER_ROW + 1) };
	
	alignments[alignmentIndex] = { priorCellIndex, hindmostCellIndex };

	removeFromNeighbours(priorCellIndex);		/* No need to process this neighbour afterward. */

	removeFromNeighbours(hindmostCellIndex);		/* No need to process this neighbour afterward. */

	furtherNeighbourSearchHelper(alignments[alignmentIndex], steps[alignmentIndex]);
}

bool Cell::isHorizontalMiddleMan()
{
	int leftHandNeighbourIndex = _index - 1;

	int rightHandNeighbourIndex = _index + 1;

	auto leftHandNeighbour = (Cell *) this->getParent()->getChildByName(std::to_string(leftHandNeighbourIndex));

	auto rightHandNeighbour = (Cell *) this->getParent()->getChildByName(std::to_string(rightHandNeighbourIndex));

	return (((leftHandNeighbour->getState() == SELECTED) || (leftHandNeighbour->getOwner() == this->getOwner()))
		&& ((rightHandNeighbour->getState() == SELECTED) || (rightHandNeighbour->getOwner() == this->getOwner())));
}

bool Cell::isVerticalMiddleMan()
{
	int topNeighbourIndex = _index + NUMBER_OF_CELLS_PER_ROW;

	int bottomNeighbourIndex = _index - NUMBER_OF_CELLS_PER_ROW;

	auto topNeighbour = (Cell *) this->getParent()->getChildByName(std::to_string(topNeighbourIndex));

	auto bottomNeighbour = (Cell *) this->getParent()->getChildByName(std::to_string(bottomNeighbourIndex));

	return (((topNeighbour->getState() == SELECTED) || (topNeighbour->getOwner() == this->getOwner()))
		&& ((bottomNeighbour->getState() == SELECTED) || (bottomNeighbour->getOwner() == this->getOwner())));
}

bool Cell::isRisingDiagonalMiddleMan()
{
	int topNeighbourIndex = _index + NUMBER_OF_CELLS_PER_ROW + 1;

	int bottomNeighbourIndex = _index - NUMBER_OF_CELLS_PER_ROW - 1;

	auto topNeighbour = (Cell *) this->getParent()->getChildByName(std::to_string(topNeighbourIndex));

	auto bottomNeighbour = (Cell *) this->getParent()->getChildByName(std::to_string(bottomNeighbourIndex));

	return (((topNeighbour->getState() == SELECTED) || (topNeighbour->getOwner() == this->getOwner()))
		&& ((bottomNeighbour->getState() == SELECTED) || (bottomNeighbour->getOwner() == this->getOwner())));
}

bool Cell::isFallingDiagonalMiddleMan()
{
	int topNeighbourIndex = _index + NUMBER_OF_CELLS_PER_ROW - 1;

	int bottomNeighbourIndex = _index - NUMBER_OF_CELLS_PER_ROW + 1;

	auto topNeighbour = (Cell *) this->getParent()->getChildByName(std::to_string(topNeighbourIndex));

	auto bottomNeighbour = (Cell *) this->getParent()->getChildByName(std::to_string(bottomNeighbourIndex));

	return (((topNeighbour->getState() == SELECTED) || (topNeighbour->getOwner() == this->getOwner()))
		&& ((bottomNeighbour->getState() == SELECTED) || (bottomNeighbour->getOwner() == this->getOwner())));
}

void Cell::furtherNeighbourSearchHelper(std::vector<int> & alignment, int step)
{
	bool done = false;

	bool checkingDown = false;

	int currentCellIndex = alignment[0] + step;

	while (!done)
	{
		if ((currentCellIndex >= 0) && (currentCellIndex <= (TOTAL_NUMBER_OF_CELLS - 1)))
		{
			auto directNeighbour = (Cell *) this->getParent()->getChildByName(std::to_string(currentCellIndex));

			/* Then check whether the cell is SELECTED or OWNED by the same owner. */
			if (((directNeighbour->getState() == SELECTED)
				|| ((directNeighbour->getState() == OWNED) && (directNeighbour->getOwner() == this->getOwner()))))
			{
				alignment.push_back(currentCellIndex);

				currentCellIndex += step;
			}

			else
			{
				if (!checkingDown)
				{
					step = -step;
					
					currentCellIndex = alignment[1] + step;			/* Look in the other direction. */

					checkingDown = true;
				}

				else
				{
					done = true;
				}
			}
		}

		else
		{
			if (!checkingDown)
			{
				step = -step;
				
				currentCellIndex = alignment[1] + step;

				checkingDown = true;
			}

			else
			{
				done = true;
			}
		}
	}
}

void Cell::takeOwnershipAction(std::map<int, std::vector<int>> & alignments)
{
	_internalCellsPosition = (alignments.begin())->second;

	if (alignments.size() > 1)
	{
		auto it = alignments.begin();
		
		for (++it; it != alignments.end(); it++)
		{
			std::copy(it->second.begin(), it->second.end(), back_inserter(_internalCellsPosition));
		}
	}

	_internalCellsPosition.push_back(_index);

	reflectOwnership();

	_internalCellsPosition = { _index };
}

void Cell::removeFromNeighbours(int cellIndex)
{	
	for (int i = 0; i < _neighbours.size(); i++)
	{
		if (_neighbours[i] == cellIndex)
		{
			_neighbours.erase((_neighbours.begin() + i), (_neighbours.begin() + i + 1));

			break;
		}
	}
}