#pragma once

#include "cocos2d.h"
#include "ui/CocosGUI.h"

class ScoreLabel : public cocos2d::Node
{
private:
	int _score;				/* Keeps track of this label score. */

	cocos2d::ui::Text * _label;

public:
	ScoreLabel(cocos2d::ui::Text * existingLabel);			/* Default constructor. */

	~ScoreLabel();			/* Destructor. */

	/*
	Creates a label from an existing one.
	*/
	static ScoreLabel * createScoreLabel(cocos2d::ui::Text * existingLabel);

	/*
	Provides a way to increase this label's score from outside.
	*/
	void increaseScore(int score);

	/*
	Provides a way to decrease this label's score from outside.
	*/
	void decreaseScore(int score);

	/*
	Returns this label's score.
	*/
	int getScore();

	/*
	A helper function
	*/
	void increaseScoreHelper();

	/*
	A helper function.
	*/
	void decreaseScoreHelper();
};