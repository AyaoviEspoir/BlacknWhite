#include "CellPanel.h"
#include "Cell.h"

USING_NS_CC;

CellPanel::CellPanel(ui::Layout * panel)
{
	_panel = panel;

	loadCells();
}

CellPanel::~CellPanel()
{

}

CellPanel * CellPanel::createPanel(ui::Layout * existingPanel)
{
	CellPanel * cellPanel = new CellPanel(existingPanel);

	return cellPanel;
}

void CellPanel::loadCells()
{
	int tracker = CELL_COUNTER;		/* Used to identify a cell by name. */
	
	for (int i = 0; i < NUMBER_OF_ROWS; i++)
	{
		int x = COLUMN_STARTING_POSITION;
		
		int y = ROW_STARTING_POSITION + i*(GAAP_BETWEEN_CELLS + WIDTH_OF_CELL);

		for (int j = 0; j < NUMBER_OF_COLUMNS; j++)
		{
			auto cell = Cell::createCell(Vec2(x,y));

			cell->setName(std::to_string(tracker));		/* Name of the cell. */

			_panel->addChild(cell);

			x += (GAAP_BETWEEN_CELLS + WIDTH_OF_CELL);
			
			++tracker;
		}
	}
}