#include "AI.h"

USING_NS_CC;

AI::AI()
{}

AI::AI(ui::Layout * grid)
{
	_grid = grid;

	_ai = new CCAI();

	_indexGenerator = new IndexGenerator();
}

AI::~AI()
{}

AI * AI::createAI(ui::Layout * grid)
{
	AI * ai = new AI(grid);

	return ai;
}