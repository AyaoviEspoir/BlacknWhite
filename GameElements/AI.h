#pragma once

#include "..\Logic\CCAI.h"
#include "..\Logic\CCIndexGenerator.h"
#include "ui/CocosGUI.h"


class AI
{
private:
	cocos2d::ui::Layout * _grid;		/* The AI perception of the board/panel. */

	CCAI * _ai;				/* Holds the logic of the AI. */

	IndexGenerator * _indexGenerator;

public:
	/*
	Default cinstructor.
	*/
	AI();

	/*
	Alternative constructor.
	*/
	AI(cocos2d::ui::Layout * existingGrid);

	/*
	Destructor.
	*/
	~AI();

	/*
	Creates an AI and returns a pointer to it.
	*/
	static AI * createAI(cocos2d::ui::Layout * existingGrid);
};