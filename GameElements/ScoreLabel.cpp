#include "ScoreLabel.h"

USING_NS_CC;

ScoreLabel::ScoreLabel(ui::Text * existingLabel)
{
	_label = existingLabel;

	_score = 0;

	_label->setString(std::to_string(_score));
}

ScoreLabel::~ScoreLabel()
{

}

ScoreLabel * ScoreLabel::createScoreLabel(ui::Text * existingPanel)
{
	ScoreLabel * scoreLabel = new ScoreLabel(existingPanel);

	return scoreLabel;
}

int ScoreLabel::getScore()
{
	return _score;
}

void ScoreLabel::increaseScore(int score)
{
	auto increaseHelperDelegate = CCCallFunc::create([&](){
		
		increaseScoreHelper();

	});

	auto increaseSequence = Sequence::create(increaseHelperDelegate, DelayTime::create(0.025f) ,NULL);

	auto repeat = Repeat::create(increaseSequence, score);

	this->runAction(repeat);
}

void ScoreLabel::decreaseScore(int score)
{
	auto decreaseHelperDelegate = CCCallFunc::create([&](){

		decreaseScoreHelper();

	});

	auto decreaseSequence = Sequence::create(decreaseHelperDelegate, DelayTime::create(0.025f), NULL);

	auto repeat = Repeat::create(decreaseSequence, score);

	this->runAction(repeat);
}

void ScoreLabel::increaseScoreHelper()
{
	++_score;

	_label->setString(std::to_string(_score));
}

void ScoreLabel::decreaseScoreHelper()
{
	--_score;

	_label->setString(std::to_string(_score));
}