#pragma once

#include "..\Utilities\Base.h"
#include <vector>

class CCell
{
private:
	/*
	Returns the neighbours of a corner cell.
	*/
	std::vector<int> getCornerCellNeighbours(int position);

	/*
	Returns the neighbours of an edge cell.
	*/
	std::vector<int> getEdgeCellNeighbours(int position);

	/*
	Returns the neighbours of an inner cell.
	*/
	std::vector<int> getInnerCellNeighbours(int position);

public:
	CCell();		/* Default constructor. */

	~CCell();		/* Destructor. */

	/*
	Returns std::vector containing the neighbours of a 
	cell given its position.
	*/
	std::vector<int> getNeighbours(int position);

	/*
	Returns true is the given cell position corresponds 
	to and a corner cell.
	*/
	bool cornerCell(int position);

	/*
	Returns true if the geven cell position corresponds 
	to an edge cell.
	*/
	bool edgeCell(int position);
};