#pragma once

#include "..\Utilities\Base.h"
#include <vector>

class CCAI
{
private:
	int _cellIndexOfChoice;

	std::vector < char** > _board;			/* Keeps a view/state of the board at all time. */

	/* 
	Initializes the board at the start of a game or whenever needed. 
	*/
	void initializeBoard();

	/*
	Computes the reward of an action.
	*/
	int computeReward();

public:
	/* 
	Default constructor. 
	*/
	CCAI();

	/* 
	Destructor. 
	*/
	~CCAI();

	/* 
	Registers the player move. 
	*/
	void registerPlayerMove(int cellIndex);

	/*
	Registers the player acquisition.
	*/
	void registerPlayerAcquisition(const std::vector<int> & cells);

	/* 
	The ultimate function. It handles the AI moves. 
	Returns the index of the cell to pick.
	*/
	int nextMove();
};