#pragma one

#include "..\Utilities\Base.h"
#include <stdio.h>      /* printf, scanf, puts, NULL */
#include <stdlib.h>     /* srand, rand */
#include <time.h>       /* time */

class IndexGenerator
{
private:
	int _index;			/* Stores the last generated index. */


public:
	/*
	Default constructor.
	*/
	IndexGenerator();

	/*
	Destructor.
	*/
	~IndexGenerator();

	/*
	Returns an index between 0-MAXIMUM_NUMBER_OF_CELLS inclusive.
	*/
	int generate();
};