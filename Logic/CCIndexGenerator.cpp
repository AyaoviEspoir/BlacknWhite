#include "CCIndexGenerator.h"

IndexGenerator::IndexGenerator()
{
	_index = 0;
}

IndexGenerator::~IndexGenerator()
{

}

int IndexGenerator::generate()
{
	srand(time(NULL));
	
	_index = rand() % TOTAL_NUMBER_OF_CELLS;

	return _index;
}