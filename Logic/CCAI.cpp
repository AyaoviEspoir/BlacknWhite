#include "CCAI.h"

CCAI::CCAI()
{
	initializeBoard();
}

CCAI::~CCAI()
{

}

void CCAI::initializeBoard()
{
	for (int rowIndex = 0; rowIndex < NUMBER_OF_CELLS_PER_ROW; rowIndex++)
	{
		char ** row = new char*[NUMBER_OF_CELLS_PER_ROW];

		for (int columnIndex = 0; columnIndex < NUMBER_OF_COLUMNS; columnIndex++)
		{
			row[columnIndex] = new char('o');
		}

		_board.push_back(row);
	}
}

void CCAI::registerPlayerMove(int cellIndex)
{
	int rowIndex = cellIndex / NUMBER_OF_CELLS_PER_ROW;

	int columnIndex = cellIndex % NUMBER_OF_COLUMNS;

	*(_board[rowIndex][columnIndex]) = 'b';
}

void CCAI::registerPlayerAcquisition(const std::vector<int> & cell)
{
	for (int cellIndex : cell)
	{
		int rowIndex = cellIndex / NUMBER_OF_CELLS_PER_ROW;

		int columnIndex = cellIndex % NUMBER_OF_COLUMNS;

		*(_board[rowIndex][columnIndex]) = 'r';
	}
}

int CCAI::nextMove()
{
	return _cellIndexOfChoice;
}