#include "CCell.h"

CCell::CCell()
{

}

CCell::~CCell()
{

}

std::vector<int> CCell::getNeighbours(int position)
{
	// Case of corner cell.
	if (cornerCell(position))
	{
		return getCornerCellNeighbours(position);
	}

	// Case of edge cell.
	if (edgeCell(position))
	{
		return getEdgeCellNeighbours(position);
	}

	// Case inner cell.
	return getInnerCellNeighbours(position);
}

bool CCell::cornerCell(int position)
{
	return ((position == BOTTOM_LEFT_CORNER) || (position == BOTTOM_RIGHT_CORNER) 
		|| (position == TOP_LEFT_CORNER) || (position == TOP_RIGH_CORNER));
}

bool CCell::edgeCell(int position)
{
	int rowID = position / NUMBER_OF_CELLS_PER_ROW;
	int columnID = position % NUMBER_OF_CELLS_PER_ROW;

	return ((columnID == LEFT_EDGE) || (columnID == RIGHT_EDGE) 
		|| (rowID == BOTTOM_ROW) || (rowID == TOP_ROW));
}

std::vector<int> CCell::getCornerCellNeighbours(int position)
{
	std::vector<int> neighbours;		/* Stores the cells neighbours. */

	// Case bottom left corner.
	if (position == BOTTOM_LEFT_CORNER)
	{
		neighbours = { 1, 21, 20 };
	}

	// case bottom riht corner.
	else if (position == BOTTOM_RIGHT_CORNER)
	{
		neighbours = { 39, 38, 18 };
	}

	// Case top left corner.
	else if (position == TOP_LEFT_CORNER)
	{
		neighbours = { 360, 361, 381 };
	}

	// Case top left corner.
	else if (position == TOP_RIGH_CORNER)
	{
		neighbours = { 398, 378, 379 };
	}

	return neighbours;
}

std::vector<int> CCell::getEdgeCellNeighbours(int position)
{
	/*
	This function assumes the cell whose position is passed 
	in as argument is not a corner cell.
	*/
	std::vector<int> neighbours;

	int rowID = position / NUMBER_OF_CELLS_PER_ROW;

	int columnID = position % NUMBER_OF_CELLS_PER_ROW;

	// Case bottom row cell.
	if (rowID == BOTTOM_ROW)
	{
		neighbours = { (columnID + 1), (columnID + NUMBER_OF_CELLS_PER_ROW + 1),
			(columnID + NUMBER_OF_CELLS_PER_ROW), (columnID + NUMBER_OF_CELLS_PER_ROW - 1), (columnID - 1) };
	}

	// Case top row cell.
	else if (rowID == TOP_ROW)
	{
		neighbours = { (rowID * NUMBER_OF_CELLS_PER_ROW + (columnID - 1)), 
			((rowID - 1) * NUMBER_OF_CELLS_PER_ROW + (columnID - 1)),
			((rowID - 1) * NUMBER_OF_CELLS_PER_ROW + columnID),
			((rowID - 1) * NUMBER_OF_CELLS_PER_ROW + (columnID + 1)),
			(rowID * NUMBER_OF_CELLS_PER_ROW + (columnID + 1)) };
	}

	// Case left edge cell.
	else if (columnID == LEFT_EDGE)
	{
		neighbours = { ((rowID - 1) * NUMBER_OF_CELLS_PER_ROW),
			((rowID - 1) * NUMBER_OF_CELLS_PER_ROW + 1),
			(rowID * NUMBER_OF_CELLS_PER_ROW + 1),
			((rowID + 1) * NUMBER_OF_CELLS_PER_ROW + 1),
			((rowID + 1) * NUMBER_OF_CELLS_PER_ROW) };
	}

	// Case right edge cell.
	else if (columnID == RIGHT_EDGE)
	{
		neighbours = { ((rowID + 1) * NUMBER_OF_CELLS_PER_ROW + columnID),
			((rowID + 1) * NUMBER_OF_CELLS_PER_ROW + (columnID - 1)),
			(rowID * NUMBER_OF_CELLS_PER_ROW + (columnID - 1)),
			((rowID - 1) * NUMBER_OF_CELLS_PER_ROW + (columnID - 1)),
			((rowID - 1) * NUMBER_OF_CELLS_PER_ROW + columnID) };
	}

	return neighbours;
}

std::vector<int> CCell::getInnerCellNeighbours(int position)
{
	int rowID = position / NUMBER_OF_CELLS_PER_ROW;

	int columnID = position % NUMBER_OF_CELLS_PER_ROW;

	std::vector<int> neighbours = { ((rowID + 1) * NUMBER_OF_CELLS_PER_ROW + columnID),
		((rowID + 1) * NUMBER_OF_CELLS_PER_ROW + (columnID - 1)),
		(rowID * NUMBER_OF_CELLS_PER_ROW + (columnID - 1)),
		((rowID - 1) * NUMBER_OF_CELLS_PER_ROW + (columnID - 1)),
		((rowID - 1) * NUMBER_OF_CELLS_PER_ROW + columnID),
		((rowID - 1) * NUMBER_OF_CELLS_PER_ROW + (columnID + 1)),
		(rowID * NUMBER_OF_CELLS_PER_ROW + (columnID + 1)),
		((rowID + 1) * NUMBER_OF_CELLS_PER_ROW + (columnID + 1)) };

	return neighbours;
}